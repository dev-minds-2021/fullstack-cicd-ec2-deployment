#!/usr/bin/env groovy
/* Declarative pipeline must be enclosed within a pipeline block */
pipeline {

    /* agent directive: determines where this entire job will run from, in our case, it will run from management-node 
       as we have just one single jenkins node, we will 
    */
    agent any

    /* options directives: adds settings to your job - each below functions are used to customize behaviour of the job */
    options {
        timestamps()
        ansiColor('xterm')
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }

    environment {

        // Terraform Stages EnvVars 
        // TF_LOG = "WARN"

        // PostStage EnvVars
        SLACK_CHANNEL = "#class_2020"
        SLACK_TOKEN = credentials("jenkins-slack-int")
        SLACK_ICON = ":white_color_mark:"
        STACK_PREFIX = "simple_webpage_proj"

        GIT_TAG_COMMIT = sh(script: 'git describe --tags --always', returnStdout: true).trim()
    }

    parameters {
        booleanParam(name: 'run_ami_build', defaultValue: false, description: 'If there is an update on the packer file, tick the box to apply new change')
        booleanParam(name: 'update_net_infra', defaultValue: false, description: 'If there are network related changes, tick the box to apply new change')
        string(name: 'deploy_to', description: 'Target environemnt to deploy: default = staging', defaultValue: 'staging')
    }

    /* stageS directive: umbrella' all stage within the pipeline */
    stages{
        /* stage directive: each stage difines what the pipeline will accomplish */
        stage('JenkinsInit'){
            /* steps directive: actual actions defined in here */ 
            steps {
                sh """
                ls -la
                hostname -f
                whoami
                echo ${GIT_TAG_COMMIT}
                """
            }
        }

        stage('InfraWorks'){
            when {
                expression { update_net_infra == 'true' }
            }
            steps {
                ansiColor('xterm'){    
                    withAWS(credentials: 'infra-creds', region: 'eu-west-1') {
                        dir("./infra/networking_resources"){
                            // sh "echo ignoring"
                            sh "~/.tfenv/bin/terraform init" 
                            // sh "~/.tfenv/bin/terraform workspace new staging"
                            sh "~/.tfenv/bin/terraform workspace select ${params.deploy_to}"
                            sh "~/.tfenv/bin/terraform workspace list"
                            // input 'Request team test, and wait for greenlight'
                            sh "~/.tfenv/bin/terraform plan -out ${params.deploy_to}.tfplan"
                            sh "~/.tfenv/bin/terraform apply ${params.deploy_to}.tfplan"
                        }
                    }
                }
            }
        }

        stage('PrepAndShareAmi') {
            when {
                expression { run_ami_build == 'true' }
            }
            steps {
                ansiColor('xterm'){    
                    withAWS(credentials: 'infra-creds', region: 'eu-west-1') {
                        dir("./infra/ami_build"){
                            sh "/usr/bin/packer build golden_image.json" 
                            sh "~/.tfenv/bin/terraform init"
                            sh "~/.tfenv/bin/terraform plan -out ami_share.tfplan"
                            sh "~/.tfenv/bin/terraform apply ami_share.tfplan"
                        }
                    }
                }
            }
        } 

        stage('ComputeWorks') {
            steps {
                ansiColor('xterm'){    
                    withAWS(credentials: 'infra-creds', region: 'eu-west-1') {
                        dir("./infra/compute_resources"){
                            sh "~/.tfenv/bin/terraform init" 
                            sh "~/.tfenv/bin/terraform workspace select ${params.deploy_to}"
                            sh "~/.tfenv/bin/terraform workspace list"
                            sh "~/.tfenv/bin/terraform plan -out ${params.deploy_to}.tfplan"
                            sh "~/.tfenv/bin/terraform apply ${params.deploy_to}.tfplan"
                            sh "sleep 750s"
                            sh "~/.tfenv/bin/terraform destroy -force" 
                        }
                    }
                }
            }
        } 


// ========== PostWorks ======== //
        stage('SlackNotifications') {
            steps {
                sh "echo JobnStatus"
            }
            post {
                success {
                    slackSend(
                            token: "${env.SLACK_TOKEN}",
                            channel: "${env.SLACK_CHANNEL}",
                            color: "good",
                            iconEmoji: "${env.SLACK_ICON}",
                            //message: "${env.STACK_PREFIX} production deploy: *${env.DEPLOY_VERSION}*. <${env.DEPLOY_URL}|Access service> - <${env.BUILD_URL}|Check build>"
                            message: "ProjectName: ${env.STACK_PREFIX}\n Environment: ${params.deploy_to} deploy: SUCCESSFUL\n Job URL: <${env.BUILD_URL}|Check build>\n BuildNumber: [${env.BUILD_NUMBER}]"
                    )
                }

                failure {
                    slackSend(
                            token: "${env.SLACK_TOKEN}",
                            channel: "${env.SLACK_CHANNEL}",
                            color: "danger",
                            message: "${env.STACK_PREFIX} ${params.deploy_to} deploy failed: FAILED. <${env.BUILD_URL}|Check build>"
                    )
                }
            }            
        }
        
    }

    post {
        always {
             deleteDir()
        }
    }
}
