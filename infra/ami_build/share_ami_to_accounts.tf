provider "aws" {
  region  = "${var.region}"
}

terraform {
  required_version = ">= 0.12.27"

  backend "s3" {
    bucket  = "vpc-states"
    key = "ami_share/terraform.tfstate"
    region  = "eu-west-1"
    encrypt = "true"
    
  }
}

variable "region" {
  description = "AWS region"
  default     = "eu-west-1"
}

# variable "ami_id" {
#   description = "EC2 AMI Id"
#   default = "ami-0086fc0fcdfdac824"
# }

variable "account_ids" {
  description = "List of Account IDs"
  type = list 
  default = ["023451010066", "984463041714"]
}

data "aws_ami" "ami_fetch" {
  most_recent = true
  owners      = ["self"]
  name_regex  = "^dm_golden_image_*"

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

resource "aws_ami_launch_permission" "share" {
  count = length(var.account_ids)
  # image_id   = "${var.ami_id}"
  image_id   = data.aws_ami.ami_fetch.id
  account_id = var.account_ids[count.index]
}

