provider "aws" {
    region = var.aws_region
}

// backend remote state setup 
terraform {
  required_version = ">= 0.12.12"

  backend "s3" {
    bucket  = "vpc-states"
    key = "terraform.tfstate"
    region  = "eu-west-1"
    encrypt = "true"
    workspace_key_prefix = "networking-resource-state"
  }
}

#############################################
# Variables and locals
############################################# 
locals {
  env_name    = lower(terraform.workspace)
  common_tags = {
    BillingCodes = var.billing_code_tag
    Environment  = local.env_name
  }
}

# Variables and definitions 
variable "aws_region" {}
variable "net_addr_space" { type = map(string) }
variable "billing_code_tag" { default = "ACCT6589513" }
variable "subnet_count" { type = map(number) }


data "aws_availability_zones" "this_ds_azs" {}


resource "random_integer" "this_random_vals" {
  min = 1000000
  max = 9999900
}

#############################################
# All resources 
############################################# 
# 1.
resource "aws_vpc" "this_vpc" {
  cidr_block = var.net_addr_space[terraform.workspace]
  tags = merge(
    local.common_tags, { Name = "${local.env_name}-vpc" }
  )
}

# 2. 
resource "aws_internet_gateway" "this_igw" {
  vpc_id = aws_vpc.this_vpc.id

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-igw" }
  )
}

# 3. 
resource "aws_subnet" "this_subnet" {
  count                   = var.subnet_count[terraform.workspace]
  cidr_block              = cidrsubnet(var.net_addr_space[terraform.workspace], 8, count.index)
  vpc_id                  = aws_vpc.this_vpc.id
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.this_ds_azs.names[count.index]

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-subnet-${count.index + 1}" }
  )
}

# 4. 
resource "aws_route_table" "this_rtb" {
  vpc_id = aws_vpc.this_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this_igw.id
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-rtb" }
  )
}

# 5.
resource "aws_route_table_association" "this_rtb_sb_assoc" {
  count          = var.subnet_count[terraform.workspace]
  subnet_id      = aws_subnet.this_subnet[count.index].id
  route_table_id = aws_route_table.this_rtb.id
}

output "net_vpc_id" {
    value = aws_vpc.this_vpc.id
}

output "net_subnet_ids" {
    value = aws_subnet.this_subnet[*].id
}