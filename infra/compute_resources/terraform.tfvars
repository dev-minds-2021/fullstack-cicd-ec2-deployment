aws_region = "eu-west-1"
key_name         = "centrale-keys"
instance_count = {
    staging = 2
#   Development = 2
# #   UAT         = 2
# #   Production  = 3
# #   prep-dev    = 3
}

instance_type = {
    staging = "t2.micro"
#   Development = "t2.micro"
#   UAT         = "t2.micro"
#   Production  = "t2.medium"
#   prep-dev    = "t2.micro"
}