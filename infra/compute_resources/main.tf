provider "aws" {
    region = var.aws_region
}

// backend remote state setup 
terraform {
  required_version = ">= 0.12.12"

  backend "s3" {
    bucket  = "vpc-states"
    key = "terraform.tfstate"
    region  = "eu-west-1"
    encrypt = "true"
    workspace_key_prefix = "compute-resource-state"
  }
}

#############################################
# Variables and locals
############################################# 
locals {
  env_name    = lower(terraform.workspace)
  common_tags = {
    BillingCodes = var.billing_code_tag
    Environment  = local.env_name
  }
}

variable "aws_region" {}
variable "key_name" {}
variable "billing_code_tag" { default = "ACCT6589513" }
variable "instance_count" { type = map(number) }
variable "instance_type" { type = map(string) }



# Dynamic fetch 
data "aws_availability_zones" "this_ds_azs" {}
data "terraform_remote_state" "net_core" {
  backend = "s3"

  config = {
    bucket = "vpc-states"
    # workspace = "staging"
    key = "networking-resource-state/staging/terraform.tfstate"
    region  = "eu-west-1"
  }
}

data "aws_ami" "ami_fetch" {
  most_recent = true
  owners      = ["self"]
  name_regex  = "^dm_golden_image_*"

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

resource "random_integer" "this_random_vals" {
  min = 1000000
  max = 9999900
}

resource "aws_security_group" "this_vm_sg" {
  name   = "${local.env_name}-nginx-server-sg"
  vpc_id = data.terraform_remote_state.net_core.outputs.net_vpc_id

   # SSH access from anywhere
  ingress {
     from_port   = 22
     to_port     = 22
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
  }

  #Allow HTTP from anyw re
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  #allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-elb-sg" }
  )
}

resource "aws_security_group" "this_elb_sg" {
  name   = "${local.env_name}-nginx_elb_sg"
  vpc_id = data.terraform_remote_state.net_core.outputs.net_vpc_id
  #Allow HTTP from anyw re
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-elb-sg" }
  )
}

resource "aws_elb" "this_elb" {
  name = "${local.env_name}-nginx-elb"

  subnets         = ["${data.terraform_remote_state.net_core.outputs.net_subnet_ids[0]}"]
  security_groups = [aws_security_group.this_elb_sg.id]
  instances       = aws_instance.this_nginx_servers[*].id

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }


  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    # target              = "HTTP:80/"
    target   = "TCP:22"
    interval = 10
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-web-elb" }
  )
}

resource "aws_instance" "this_nginx_servers" {
  count                  = var.instance_count[terraform.workspace]
  ami                    = data.aws_ami.ami_fetch.id
  instance_type          = var.instance_type[terraform.workspace]
  subnet_id              = data.terraform_remote_state.net_core.outputs.net_subnet_ids[0]
  vpc_security_group_ids = [aws_security_group.this_vm_sg.id]
  key_name               = var.key_name
#   iam_instance_profile   = aws_iam_instance_profile.nginx_profile.name
#   depends_on             = [aws_iam_role_policy.allow_s3_all]
}

output "aws_elb_public_dns" {
  value = aws_elb.this_elb.dns_name
}

output "aaws_instance_pub_ip" {
  value = aws_instance.this_nginx_servers[*].public_ip
}